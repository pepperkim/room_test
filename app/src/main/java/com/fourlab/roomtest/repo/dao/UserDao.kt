package com.fourlab.roomtest.repo.dao

import androidx.room.*
import com.fourlab.roomtest.repo.entity.UserEntity

@Dao
abstract class UserDao {

    @Query("select * from userentity" )
    abstract fun findAll():List<UserEntity>

    @Query("select * from userentity where id = :id")
    abstract fun findOneId(id: Long):UserEntity

    @Insert
    abstract fun insert(userEntity: UserEntity):Long

    @Update
    abstract fun update(userEntity: UserEntity):Int

    @Delete
    abstract fun delete(userEntity: UserEntity):Int

    @Query("delete from userentity where id = :id")
    abstract fun deleteById(id: Long):Int

}