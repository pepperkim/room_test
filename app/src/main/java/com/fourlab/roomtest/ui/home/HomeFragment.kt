package com.fourlab.roomtest.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fourlab.roomtest.R
import com.fourlab.roomtest.databinding.FragmentHomeBinding
import com.fourlab.roomtest.repo.dao.UserDao
import com.fourlab.roomtest.repo.database.UserDB
import com.fourlab.roomtest.repo.entity.UserEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import timber.log.Timber

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textHome
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        textView.setOnClickListener {
            getAllUser()
        }
        binding.btnInsert.setOnClickListener {
            insertUser()
        }

        binding.btnUpdate.setOnClickListener {
            updateUser()
        }

        binding.btnDelete.setOnClickListener {
            deleteUser()
        }

        return root
    }

    private fun getAllUser(){
        CoroutineScope(Dispatchers.IO).launch {
            val id = UserDB.db.userDao().runCatching { findAll()}
                .onFailure { Timber.wtf(it) }
                .getOrNull()
            Timber.d("alluser : $id")
        }
    }

    private fun insertUser(){
        CoroutineScope(Dispatchers.IO).launch {
            val id = UserDB.db.userDao().runCatching { insert(
                UserEntity("master", 1,"master")
            )}
                .onFailure { Timber.wtf(it) }
                .getOrNull()
            Timber.d("insert user : $id")
        }
    }

    private fun deleteUser(){
        CoroutineScope(Dispatchers.IO).launch {
            val id = UserDB.db.userDao().runCatching { deleteById(1) }
                .onFailure { Timber.wtf(it) }
                .getOrNull()
            Timber.d("delete user : $id")
        }
    }

    private fun updateUser(){
        CoroutineScope(Dispatchers.IO).launch {
//            try {
                UserDB.db.userDao().runCatching { findOneId(3) }
                    .onFailure { Timber.wtf("findOneId user : ${it.stackTrace}") }
                    .onSuccess { user->
                        user.name = "yoda"
                        UserDB.db.userDao().runCatching { update(user) }
                            .onSuccess { Timber.d("update user : $it") }
                            .onFailure { Timber.wtf("update user : ${it.stackTrace}") }
                    }


//            }catch (err:NullPointerException){
//                Timber.wtf("NullPointerException ${err.stackTrace}")
//            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}