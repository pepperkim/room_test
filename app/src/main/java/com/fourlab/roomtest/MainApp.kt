package com.fourlab.roomtest

import android.app.Application
import android.content.Context
import timber.log.Timber
import java.lang.ref.WeakReference

class MainApp : Application() {

    companion object {
        private lateinit var contextRef: WeakReference<Context>
        val context: Context
            get() = contextRef.get()!!
    }

    override fun onCreate() {
        super.onCreate()
        contextRef = WeakReference(this.applicationContext)

        object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return super.createStackElementTag(element)
                    ?.let { "[${Thread.currentThread().name}] $it" }
            }
        }.let { Timber.plant(it) }
    }
}